# Assignment


## Assigment 1: 27th Sep 2015, midnight

Design an algorithm to

1. Print the word count of a file.

2. print distinct words in the file

3. Give a frequency count of words in the file. You should ignore
   noise words like a, and, the etc. I leave you to design the corpus
   of noise words.

Hint: Write the corresponding functions for a string and the in the
main function apply it on the contents of the file read lazily. Use
the `hGetContents` functions
