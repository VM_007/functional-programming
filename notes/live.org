#+STARTUP: indent

* Introduction

** Basic information

  Functional programming in Haskell.

  The class will be done in /flipped classroom/ mode

  1. There will video lectures uploaded (weekly)

  2. We will discuss this lecture in the live session.

  3. Evaluation will be based on Quizes (best n of n+1).
     End Sem (75% Q + 25 % E).


  We have

  - Moodle :: Starting from here you should be able to get
    to all resources of this course.

  - Course repository :: Notes and sample code

    https://bitbucket.org/piyush-kurur/functional-programming/

  - Discord Channel :: https://discord.gg/xpX7FJG2VJ

  - Meet link :: https://meet.google.com/fxj-joqy-shr




** Functional programming with emphasis in using

   - Basics :: Functional programming.

   - Haskell ::

     1. Purely functional instead of mostly functional (scheme, SML, ocaml etc)

     2. Lazy evaluation :: The language evaluates something only if it is necessary
	Haskell supports lazy evaluation by default as opposed to eager evaluation.
	Take the case where you are evaluating a function ~f e1 e2~

	In an eager evaluation language you evaluate ~e1~ to ~v1~ and
        ~e2~ to ~v2~ and then evaluate ~f~ on ~v1~ and ~v2~.

     3. Purely functional idea is taken to its extreem, the language
        has no side effect. Side effects are tracked in the type
        system.

	#+begin_src c
	    #include<stdio.h>

	    int main ( )
	    { int c = getchar();
	      int cp = getchar();
	      int a  = foo();
	      int b  = foo();
	      char *p = "hello";
	      a = p + b;
	      /* cp and c are equal */

	     ....

	      if ( fp = fopen("...") = NULL)
	      {

	      }

	    }


	  double av(double *arr, size_t sz)
	  {
	       double sum = 0;
	       for(int i =0; i < sz ; ++i)
	       {
		  sum += arr[i];
	       }
               return 1/sz * sum;
	  }

	  /* This is inside a library */

	   int foo ()
	   {   return 42;
	   }


	#+end_src


  |--------+--------------+--------------------|
  |        | Dynamic      | Static             |
  |--------+--------------+--------------------|
  | Strong | python, Ruby | SML, Haskell, Java |
  |--------+--------------+--------------------|
  | Weak   | JS           | C                  |
  |--------+--------------+--------------------|
