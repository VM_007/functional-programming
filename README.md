# Functional programming

This is the course repository for my [Functional programming] course.


* [Course Homepage][functional programming]

* [Course Wiki][wiki]

* [Issue tracker][issues]

For all queries and clarification please raise issues on the issue
tracker. Also use the issue tracker as a discussion board for the
course.

## Corona Special

I am in the process of preparing a set of online lectures. It will be
in the "Flipped" classroom model where a set of 1-way video lectures
will be uploaded and a live session will be held separately.


0. [Overview][corona-overview]

The complete playlist will be available through [Functional
programming][fp-video] collection of [my vimeo account][my-vimeo]

During the video I use the emacs buffer as the black board. This
transcript is available as the file `src/corona/transcript.org`
in this repository.

[fp-video]: <https://vimeo.com/showcase/7126837>
[my-vimeo]: <https://vimeo.com/piyushkurur/>
[corona-overview]: <https://vimeo.com/showcase/7126837/video/418006711> "Lecture 0: Overview"
[functional programming]: <http://cse.iitk.ac.in/users/ppk/teaching/Functional-Programming/index.html>
[wiki]: <https://bitbucket.org/ppk-teach/functional-programming/wiki>
[issues]: <https://bitbucket.org/ppk-teach/functional-programming/issues>
