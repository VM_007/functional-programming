#!/bin/bash
#
# This scripts can be used to download the lectures. It uses the
# program youtube-dl
# <https://ytdl-org.github.io/youtube-dl/index.html> (package exists
# for Debian/Ubuntu)
#
# Despite the name works with vimeo as well so no worries.
#

#
# usage:
# ./download.sh url-to-the-lecture
#
# If you do not give any arguments it takes it from your cut buffer. SO
# you can just cut the url in your browser and then run
#
# ./download.sh # no arguments required here.
#



url=$1

if [ -z "$1" ]
then
    url=`xclip -selection clipboard -o`
fi

youtube-dl -o '%(title)s-%(alt-title)s.%(ext)s' "$url"
